from flask import Flask
from socket import gethostname
import os

from pymemcache.client import base
from flask_restful import Resource, Api

application = Flask(__name__)
api = Api(application)

count_cached = 0
client = base.Client(('192.168.99.110', 11211))

@application.route('/')
def hello():
    some_links = """
    <p>
    Links:
    <li><a href='/counter'>counter + 1</a></li>
    <li><a href='set_cache'>cleans cache</a></li>
    </p>
    <p>REST:
    <li><a href='api/counter'>api/counter</a></li>
    <li><a href='api/test'>api/test</a></li>
    </p>
    """
    return '{}'.format(some_links)

@application.route('/set_cache')
def reset():
    client.set('counter', str(0))
    return 'Cache empty'


@application.route('/counter')
def counter():
    try:
        count_cached = int(client.get('counter'))
    except TypeError:
        client.set('counter', str(0))
        return "Cache failed and it was reset to 0<br> ...don't ask why xD"
    except NameError:
        return "Memcached server unreacheable"
    the_count = count_cached
    count_cached += 1
    client.set('counter', str(count_cached))
    return 'seen: {0} times <br> {1}'.format(
                                    count_cached,
                                    gethostname())


class Test(Resource):
    def get(self):
        return {'running': 'True'}


class TestCounter(Resource):
    def get(self):
        try:
            count_cached = int(client.get('counter'))
            return {'CacheCount': count_cached}
        except NameError:
            return {'CacheCount': None,
                    'Reason': 'Memcached server unreacheable'}


# api.add_resource(HelloWorld, '/api/')
api.add_resource(Test, '/api/test')
api.add_resource(TestCounter, '/api/counter')


if __name__ == "__main__":
    application.run(host="0.0.0.0", debug=True)
